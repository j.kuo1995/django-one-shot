from django.shortcuts import render, get_object_or_404, redirect
from todos.forms import TodoListForm
from todos.models import TodoList, TodoItem


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            create = form.save()
            return redirect("todo_list_detail", id=create.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def update_todo(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list", id=id)
        else:
            form = TodoListForm(instance=todo)

        context = {
            "todo_object": todo,
            "form": form,
        }
        return render(request, "todos/edit.html", context)
